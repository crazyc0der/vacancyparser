﻿using DB;
using DB.GeneratedModels;
using DB.Models;
using HtmlAgilityPack;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataParser
{
    public sealed class Processing
    {
        ConcurrentBag<string> addedCompanyIds = new ConcurrentBag<string>();

        public async Task ProcessRawData(string rawData, string pageInfo)
        {
            var document = new HtmlDocument();
            document.LoadHtml(rawData);

            //получаем строку с json-объектом, содержащим информацию о вакансиях
            var node = document.DocumentNode.SelectNodes("//script").Where(n => n.InnerHtml.Contains("SEARCH_DATA"));
            if (node.Count() > 0)
            {
                var rawText = node.First().InnerText.Trim();
                //Обрезаем строку, оставляя подстроку с json-объектом, содержащим информацию о вакансиях
                var rawVacanciesText = rawText.Substring(rawText.IndexOf("vacancies") - 2);
                //Обрезаем строку с конца там, где заканчивается json-объект LIST
                rawVacanciesText = rawVacanciesText.Substring(0, rawVacanciesText.IndexOf("var REGION_ID")).Trim();
                var vacanciesData = rawVacanciesText.Substring(0, rawVacanciesText.Length-1);
                try
                {
                    var vacancies = JsonConvert.DeserializeObject<VacanciesContainer>(vacanciesData, new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    });
                    Console.WriteLine(pageInfo);
                    await Save(vacancies);
                }
                catch(Exception ex)
                {
                    Console.WriteLine(pageInfo + " has not been processed");
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private async Task Save(VacanciesContainer container)
        {
            //Для упрощения структуры БД и уменьшения объема работы сделал для БД другие модели, описывающие вакансии
            using (var context = new VPContext())
            {
                foreach (var item in container.Vacancies)
                {
                    if (context.Vacancies.FirstOrDefault(v => v.Id == item.Id) != null)
                        continue;

                    //Без привязки к компании не добавляем
                    if (item.Company == null || string.IsNullOrWhiteSpace(item.Company.Id))
                        continue;

                    if (item.Contact == null)
                        continue;

                    var vacancy = new DB.Models.Vacancy();
                    vacancy.AddDate = item.AddDate;

                    if (item.Address != null)
                        vacancy.Address = string.Format("{0}, {1}, {2}", item.Address.City.Title, item.Address.Street, item.Address.Building);

                    vacancy.Bonus = item.Bonus;

                    var company = context.Companies.Where(c => c.Id == item.Company.Id).FirstOrDefault();
                    if (company != null)
                    {
                        vacancy.Company = company;
                    }
                    else if (!addedCompanyIds.Contains(item.Company.Id))
                    {
                        vacancy.Company = new DB.Models.Company()
                        {
                            Id = item.Company.Id,
                            State = item.Company.State,
                            Title = item.Company.Title,
                            ValidateState = item.Company.ValidateState
                        };
                        addedCompanyIds.Add(item.Company.Id);
                    }

                    if (item.Contact != null)
                    {
                        vacancy.Contact = new DB.Models.Contact()
                        {
                            Address = item.Contact.Address,
                            Building = item.Contact.Building,
                            City = item.Contact.City.Title,
                            Email = item.Contact.Email,
                            Icq = item.Contact.Icq,
                            Name = item.Contact.Name,
                            Skype = item.Contact.Skype,
                            Street = item.Contact.Street,
                            Id = Guid.NewGuid().ToString()
                        };
                    }

                    vacancy.Description = item.Description;

                    if (item.Education != null)
                        vacancy.Education = item.Education.Title;

                    if (item.ExperienceLength != null)
                        vacancy.ExperienceLength = item.ExperienceLength.Title;

                    vacancy.Header = item.Header;
                    vacancy.Id = item.Id;
                    vacancy.ModDate = item.ModDate;

                    if (item.Owner != null)
                        vacancy.Owner = item.Owner.Type;
                    vacancy.OwnerId = item.OwnerId;
                    vacancy.Requirements = item.Requirements;

                    context.Vacancies.Add(vacancy);
                }

                try
                {
                    await context.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Data for page with parameters limit={0} and offset={1} has not been saved", container.Metadata.Resultset.Limit, container.Metadata.Resultset.Offset);
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
