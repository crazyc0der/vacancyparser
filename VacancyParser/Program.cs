﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebLoader;

namespace VacancyParser
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () =>
            {
                Console.WriteLine("Parser ekb.zarplata.ru was started");
                MainLinksLoader loader = new MainLinksLoader("https://ekb.zarplata.ru/vacancy");
                var links = await loader.GetLinks();
                VacancyLoader vac = new VacancyLoader();
                await vac.LoadListPages(links);
                Console.WriteLine("Press any key to exit");
                Console.ReadKey();
            }).Wait();
            
        }
    }
}
