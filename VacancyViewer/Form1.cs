﻿using DataGridViewAutoFilter;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace VacancyViewer
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        //Архитектура и логика вывода данных и фифльтрации предельно упрощены, т.к других действий не требуется
        private void MainForm_Load(object sender, EventArgs e)
        {
            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[2];
            using (var connection = new SqlConnection(settings.ConnectionString))
            {
                BindingSource bs = new BindingSource();
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();

                string query = "SELECT Vacancies.AddDate, Vacancies.Header, Vacancies.Education, Vacancies.ExperienceLength, Vacancies.Description, Vacancies.ModDate, Vacancies.Requirements, Vacancies.Company_Id, Vacancies.Contact_Id, Companies.Title, Contacts.Name, Contacts.Address FROM Vacancies INNER JOIN Contacts ON Vacancies.Contact_Id = Contacts.Id INNER JOIN Companies ON Vacancies.Company_Id = Companies.Id";

                var command = new SqlCommand(query, connection);
                connection.Open();

                ds.Tables.Add(dt);
                using (var da = new SqlDataAdapter())
                {
                    da.SelectCommand = command;
                    da.Fill(dt);
                    bs.DataSource = dt;
                    dataGridView1.DataSource = bs;
                }
            }

            DataGridViewColumn filteredColumn1 = dataGridView1.Columns[1];
            filteredColumn1.HeaderCell = new DataGridViewAutoFilterColumnHeaderCell(filteredColumn1.HeaderCell);

            DataGridViewColumn filteredColumn2 = dataGridView1.Columns[2];
            filteredColumn2.HeaderCell = new DataGridViewAutoFilterColumnHeaderCell(filteredColumn2.HeaderCell);

            DataGridViewColumn filteredColumn3 = dataGridView1.Columns[3];
            filteredColumn3.HeaderCell = new DataGridViewAutoFilterColumnHeaderCell(filteredColumn3.HeaderCell);
        }
    }
}
