﻿using DB.GeneratedModels;
using HtmlAgilityPack;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace WebLoader
{
    public sealed class MainLinksLoader
    {
        public string StartPageUrl { get; private set; }
        public const int DefaultPageSize = 50;

        public MainLinksLoader(string startPageUrl)
        {
            StartPageUrl = startPageUrl;
        }

        public async Task<List<string>> GetLinks()
        {
            var navigationData = await LoadNavigationData();
            Console.WriteLine("Navigation data was parsed");
            return GenerateLinksForVacancyPages(navigationData);
        }

        private async Task<CriterionsContainer> LoadNavigationData(int pageSize = DefaultPageSize)
        {
            using (var client = new HttpClient())
            {
                var url = string.Format("{0}?limit={1}", StartPageUrl, pageSize);
                var rawHtml = await client.GetStringAsync(url);
                var document = new HtmlDocument();
                document.LoadHtml(rawHtml);
                //получаем строку с json-объектом, содержащим информацию для навигации
                var node = document.DocumentNode.SelectNodes("//script").Where(n => n.InnerHtml.Contains("SEARCH_DATA"));
                if (node.Count() > 0)
                {
                    var rawText = node.First().InnerText.Trim();
                    //Нужно получить значение переменной SEARCH_DATA с информацией для навигации
                    var rawData = rawText.Substring(rawText.IndexOf('{'));
                    //Обрезаем строку с конца там, где заканчивается json-объект SEARCH_DATA
                    var searchData = rawData.Substring(0, rawData.IndexOf("'"));
                    return JsonConvert.DeserializeObject<CriterionsContainer>(searchData);
                }
                else
                    return new CriterionsContainer();
            }
        }

        private List<string> GenerateLinksForVacancyPages(CriterionsContainer navigationData)
        {
            var links = new List<string>();
            int offset = 0;

            for (var page = 0; page < navigationData.PageCount; page++)
            {
                var link = string.Format("{0}?limit={1}&offset={2}", StartPageUrl, navigationData.Limit, offset);
                offset += navigationData.Limit;
                links.Add(link);
            }

            Console.WriteLine("Links for parsing vacancies was generated");
            return links;
        }
    }
}
