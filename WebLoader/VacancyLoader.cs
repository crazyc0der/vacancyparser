﻿using DataParser;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WebLoader
{
    public sealed class VacancyLoader
    {
        const int MAX_CONNECTIONS = 20;

        public async Task LoadListPages(List<string> TargetSiteLinks)
        {
            Console.WriteLine("Processing pages:");
            Processing processing = new Processing();

            using (var semaphore = new SemaphoreSlim(MAX_CONNECTIONS))
            using (var client = new HttpClient())
            {
                var tasks = TargetSiteLinks.Select(async url =>
                {
                    await semaphore.WaitAsync();
                    try
                    {
                        var data = await client.GetStringAsync(url);
                        await processing.ProcessRawData(data, url);
                    }
                    finally
                    {
                        semaphore.Release();
                    }
                });

                await Task.WhenAll(tasks);
                Console.WriteLine("Processing complete");
            }

        }
    }
}
