﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.Models
{
    public class Vacancy
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public DateTime AddDate { get; set; }

        public int OwnerId { get; set; }

        public string Header { get; set; }

        public int Bonus { get; set; }

        public string Education { get; set; }

        public string ExperienceLength { get; set; }

        public string WorkingType { get; set; }

        public string Description { get; set; }

        public virtual Contact Contact { get; set; }

        public string Address { get; set; }

        public virtual Company Company { get; set; }

        public DateTime ModDate { get; set; }

        public string Owner { get; set; }

        public int SalaryMinRub { get; set; }

        public int SalaryMaxRub { get; set; }

        public string Requirements { get; set; }

        public string Salary { get; set; }


    }
}
