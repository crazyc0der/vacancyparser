﻿using System.Collections.Generic;

namespace DB.Models
{
    public class Contact
    {
        public string Id { get; set; }

        public int Icq { get; set; }

        public string Skype { get; set; }

        public object Email { get; set; }

        public string Street { get; set; }

        public string Building { get; set; }

        public string Name { get; set; }

        public string City { get; set; }

        public string Address { get; set; }

        public virtual List<Vacancy> Vacancies { get; set; }
    }
}
