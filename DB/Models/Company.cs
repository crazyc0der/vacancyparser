﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.Models
{
    public class Company
    {
        public Company()
        {
            Vacancies = new List<Vacancy>();
        }

        public string Id { get; set; }

        public string Title { get; set; }

        public int State { get; set; }

        public int ValidateState { get; set; }

        public virtual ICollection<Vacancy> Vacancies { get; set; }
    }
}
