﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DB.GeneratedModels
{
    [DataContract]
    public class Currency
    {

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "alias")]
        public string Alias { get; set; }

    }

    [DataContract]
    public class Education
    {

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "alias")]
        public object Alias { get; set; }

    }

    [DataContract]
    public class ExperienceLength
    {

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

    }

    [DataContract]
    public class WorkingType
    {

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "alias")]
        public object Alias { get; set; }

    }

    [DataContract]
    public class Schedule
    {

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "alias")]
        public object Alias { get; set; }

    }

    [DataContract]
    public class ProlongData
    {

        [DataMember(Name = "id")]
        public object Id { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "orderId")]
        public object OrderId { get; set; }

    }

    [DataContract]
    public class Publication
    {

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "geo_id")]
        public int GeoId { get; set; }

        [DataMember(Name = "order_id")]
        public int? OrderId { get; set; }

        [DataMember(Name = "vacancy_id")]
        public int VacancyId { get; set; }

        [DataMember(Name = "company_id")]
        public int CompanyId { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "action")]
        public string Action { get; set; }

        [DataMember(Name = "is_active")]
        public bool IsActive { get; set; }

        [DataMember(Name = "is_free")]
        public bool IsFree { get; set; }

        [DataMember(Name = "logo_in_list")]
        public bool LogoInList { get; set; }

        [DataMember(Name = "is_service_finished")]
        public bool IsServiceFinished { get; set; }

        [DataMember(Name = "live_time")]
        public int LiveTime { get; set; }

        [DataMember(Name = "expired_at")]
        public DateTime ExpiredAt { get; set; }

        [DataMember(Name = "published_at")]
        public DateTime PublishedAt { get; set; }

        [DataMember(Name = "created_at")]
        public DateTime CreatedAt { get; set; }

        [DataMember(Name = "is_dummy")]
        public bool IsDummy { get; set; }

        [DataMember(Name = "is_deleted")]
        public bool IsDeleted { get; set; }

        [DataMember(Name = "prolong_data")]
        public ProlongData ProlongData { get; set; }
    }

    [DataContract]
    public class City
    {

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "locative")]
        public string Locative { get; set; }
    }

    [DataContract]
    public class Coordinate
    {

        [DataMember(Name = "lat")]
        public double Lat { get; set; }

        [DataMember(Name = "lon")]
        public double Lon { get; set; }
    }

    [DataContract]
    public class Subway
    {

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }
    }

    [DataContract]
    public class District
    {

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }
    }

    [DataContract]
    public class Microdistrict
    {

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }
    }

    [DataContract]
    public class Contact
    {

        [DataMember(Name = "icq")]
        public int Icq { get; set; }

        [DataMember(Name = "skype")]
        public string Skype { get; set; }

        [DataMember(Name = "email")]
        public object Email { get; set; }

        [DataMember(Name = "url")]
        public string Url { get; set; }

        [DataMember(Name = "street")]
        public string Street { get; set; }

        [DataMember(Name = "building")]
        public string Building { get; set; }

        [DataMember(Name = "room")]
        public object Room { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "firstname")]
        public object Firstname { get; set; }

        [DataMember(Name = "lastname")]
        public object Lastname { get; set; }

        [DataMember(Name = "patronymic")]
        public object Patronymic { get; set; }

        [DataMember(Name = "phones")]
        public IList<object> Phones { get; set; }

        [DataMember(Name = "city")]
        public City City { get; set; }

        [DataMember(Name = "address_description")]
        public string AddressDescription { get; set; }

        [DataMember(Name = "coordinate")]
        public Coordinate Coordinate { get; set; }

        [DataMember(Name = "address")]
        public string Address { get; set; }

        [DataMember(Name = "subway")]
        public Subway Subway { get; set; }

        [DataMember(Name = "district")]
        public District District { get; set; }

        [DataMember(Name = "microdistrict")]
        public Microdistrict Microdistrict { get; set; }
    }

    [DataContract]
    public class Address
    {

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "company_id")]
        public int CompanyId { get; set; }

        [DataMember(Name = "city_id")]
        public int CityId { get; set; }

        [DataMember(Name = "city")]
        public City City { get; set; }

        [DataMember(Name = "street")]
        public string Street { get; set; }

        [DataMember(Name = "building")]
        public string Building { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "coordinate")]
        public Coordinate Coordinate { get; set; }
    }

    [DataContract]
    public class PaymentType
    {

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "alias")]
        public string Alias { get; set; }
    }

    [DataContract]
    public class Logo
    {

        [DataMember(Name = "id")]
        public string Id { get; set; }

        [DataMember(Name = "url")]
        public string Url { get; set; }

        [DataMember(Name = "size")]
        public object Size { get; set; }
    }

    [DataContract]
    public class Photo
    {

        [DataMember(Name = "id")]
        public string Id { get; set; }

        [DataMember(Name = "image_id")]
        public string ImageId { get; set; }

        [DataMember(Name = "url")]
        public string Url { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "created_at")]
        public DateTime CreatedAt { get; set; }

        [DataMember(Name = "likes_count")]
        public int LikesCount { get; set; }

        [DataMember(Name = "views_count")]
        public int ViewsCount { get; set; }

        [DataMember(Name = "is_moderated")]
        public bool IsModerated { get; set; }
    }

    [DataContract]
    public class Employees
    {

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }
    }

    [DataContract]
    public class Reviews
    {

        [DataMember(Name = "average")]
        public double Average { get; set; }

        [DataMember(Name = "count")]
        public int Count { get; set; }
    }

    [DataContract]
    public class Company
    {

        [DataMember(Name = "id")]
        public string Id { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "logo")]
        public Logo Logo { get; set; }

        [DataMember(Name = "interviews")]
        public IList<object> Interviews { get; set; }

        [DataMember(Name = "onedays")]
        public IList<object> Onedays { get; set; }

        [DataMember(Name = "photos")]
        public IList<Photo> Photos { get; set; }

        [DataMember(Name = "show_logo")]
        public bool? ShowLogo { get; set; }

        [DataMember(Name = "state")]
        public int State { get; set; }

        [DataMember(Name = "validate_state")]
        public int ValidateState { get; set; }

        [DataMember(Name = "employees")]
        public Employees Employees { get; set; }

        [DataMember(Name = "reviews")]
        public Reviews Reviews { get; set; }
    }

    //[DataContract]
    //public class Photo
    //{

    //    [DataMember(Name = "url")]
    //    public string Url { get; set; }

    //    [DataMember(Name = "id")]
    //    public string Id { get; set; }
    //}

    [DataContract]
    public class Owner
    {

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "photo")]
        public Photo Photo { get; set; }
    }

    [DataContract]
    public class Toponym
    {

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "names")]
        public IList<string> Names { get; set; }
    }

    [DataContract]
    public class Speciality
    {

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }
    }

    [DataContract]
    public class Rubric
    {

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "specialities")]
        public IList<Speciality> Specialities { get; set; }
    }

    [DataContract]
    public class Vacancy
    {

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "add_date")]
        public DateTime AddDate { get; set; }

        [DataMember(Name = "owner_id")]
        public int OwnerId { get; set; }

        [DataMember(Name = "header")]
        public string Header { get; set; }

        [DataMember(Name = "state")]
        public int State { get; set; }

        [DataMember(Name = "validate_state")]
        public int ValidateState { get; set; }

        [DataMember(Name = "visibility_type")]
        public string VisibilityType { get; set; }

        [DataMember(Name = "removal")]
        public bool Removal { get; set; }

        [DataMember(Name = "site_id")]
        public int SiteId { get; set; }

        [DataMember(Name = "bonus")]
        public int Bonus { get; set; }

        [DataMember(Name = "salary_min")]
        public int SalaryMin { get; set; }

        [DataMember(Name = "salary_max")]
        public int SalaryMax { get; set; }

        [DataMember(Name = "currency")]
        public Currency Currency { get; set; }

        [DataMember(Name = "education")]
        public Education Education { get; set; }

        [DataMember(Name = "experience_length")]
        public ExperienceLength ExperienceLength { get; set; }

        [DataMember(Name = "working_type")]
        public WorkingType WorkingType { get; set; }

        [DataMember(Name = "schedule")]
        public Schedule Schedule { get; set; }

        [DataMember(Name = "publication")]
        public Publication Publication { get; set; }

        [DataMember(Name = "user_type")]
        public string UserType { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "contact")]
        public Contact Contact { get; set; }

        [DataMember(Name = "address")]
        public Address Address { get; set; }

        [DataMember(Name = "payment_type_alias")]
        public string PaymentTypeAlias { get; set; }

        [DataMember(Name = "payment_type")]
        public PaymentType PaymentType { get; set; }

        [DataMember(Name = "company")]
        public Company Company { get; set; }

        [DataMember(Name = "mod_date")]
        public DateTime ModDate { get; set; }

        [DataMember(Name = "views")]
        public int Views { get; set; }

        [DataMember(Name = "is_anonymous")]
        public bool IsAnonymous { get; set; }

        [DataMember(Name = "owner")]
        public Owner Owner { get; set; }

        [DataMember(Name = "is_moderated")]
        public bool IsModerated { get; set; }

        [DataMember(Name = "imported_via")]
        public object ImportedVia { get; set; }

        [DataMember(Name = "subways")]
        public IList<object> Subways { get; set; }

        [DataMember(Name = "tags")]
        public IList<object> Tags { get; set; }

        [DataMember(Name = "institutions")]
        public IList<object> Institutions { get; set; }

        [DataMember(Name = "jobs")]
        public IList<object> Jobs { get; set; }

        [DataMember(Name = "show_email")]
        public bool ShowEmail { get; set; }

        [DataMember(Name = "show_phone")]
        public bool ShowPhone { get; set; }

        [DataMember(Name = "use_messages")]
        public bool UseMessages { get; set; }

        [DataMember(Name = "is_commerce")]
        public bool IsCommerce { get; set; }

        [DataMember(Name = "is_upped")]
        public bool IsUpped { get; set; }

        [DataMember(Name = "is_premium")]
        public bool IsPremium { get; set; }

        [DataMember(Name = "log_state_update")]
        public object LogStateUpdate { get; set; }

        [DataMember(Name = "toponyms")]
        public IList<Toponym> Toponyms { get; set; }

        [DataMember(Name = "favorite")]
        public bool Favorite { get; set; }

        [DataMember(Name = "hided")]
        public bool Hided { get; set; }

        [DataMember(Name = "url")]
        public string Url { get; set; }

        [DataMember(Name = "salary_min_rub")]
        public int SalaryMinRub { get; set; }

        [DataMember(Name = "salary_max_rub")]
        public int SalaryMaxRub { get; set; }

        [DataMember(Name = "rubrics")]
        public IList<Rubric> Rubrics { get; set; }

        [DataMember(Name = "requirements_short")]
        public string RequirementsShort { get; set; }

        [DataMember(Name = "requirements")]
        public string Requirements { get; set; }

        [DataMember(Name = "priority")]
        public int Priority { get; set; }

        [DataMember(Name = "salary")]
        public string Salary { get; set; }

        [DataMember(Name = "salary_formatted")]
        public string SalaryFormatted { get; set; }

        [DataMember(Name = "can_accept_replies")]
        public bool CanAcceptReplies { get; set; }
    }

    [DataContract]
    public class Query
    {

        [DataMember(Name = "state")]
        public IList<int> State { get; set; }

        [DataMember(Name = "average_salary")]
        public string AverageSalary { get; set; }

        [DataMember(Name = "city_id")]
        public string CityId { get; set; }

        [DataMember(Name = "entity")]
        public string Entity { get; set; }

        [DataMember(Name = "geo_id")]
        public IList<int> GeoId { get; set; }

        [DataMember(Name = "search_type")]
        public string SearchType { get; set; }
    }

    [DataContract]
    public class Deprecation
    {

        [DataMember(Name = "code")]
        public string Code { get; set; }

        [DataMember(Name = "field")]
        public string Field { get; set; }

        [DataMember(Name = "message")]
        public string Message { get; set; }
    }

    [DataContract]
    public class Resultset
    {

        [DataMember(Name = "count")]
        public int Count { get; set; }

        [DataMember(Name = "limit")]
        public int Limit { get; set; }

        [DataMember(Name = "offset")]
        public int Offset { get; set; }
    }

    [DataContract]
    public class Metadata
    {

        [DataMember(Name = "query")]
        public Query Query { get; set; }

        [DataMember(Name = "average_salary")]
        public int AverageSalary { get; set; }

        [DataMember(Name = "deprecations")]
        public IList<Deprecation> Deprecations { get; set; }

        [DataMember(Name = "resultset")]
        public Resultset Resultset { get; set; }
    }

    [DataContract]
    public class VacanciesContainer
    {

        [DataMember(Name = "vacancies")]
        public IList<Vacancy> Vacancies { get; set; }

        [DataMember(Name = "metadata")]
        public Metadata Metadata { get; set; }
    }
}
