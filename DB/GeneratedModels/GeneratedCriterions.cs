﻿using System.Collections.Generic;
using System.Runtime.Serialization;

/// <summary>
/// Объекты сгенерированы автоматически
/// </summary>
namespace DB.GeneratedModels
{

    [DataContract]
    public class Criterions
    {

        [DataMember(Name = "state")]
        public IList<int> State { get; set; }

        [DataMember(Name = "average_salary")]
        public string AverageSalary { get; set; }

        [DataMember(Name = "city_id")]
        public object CityId { get; set; }

        [DataMember(Name = "entity")]
        public int Entity { get; set; }

        [DataMember(Name = "geo_id")]
        public IList<int> GeoId { get; set; }

        [DataMember(Name = "search_type")]
        public string SearchType { get; set; }

        [DataMember(Name = "q")]
        public string Q { get; set; }

        [DataMember(Name = "searched_q")]
        public string SearchedQ { get; set; }
    }

    [DataContract]
    public class Sorting
    {

        [DataMember(Name = "cnt")]
        public string Cnt { get; set; }
    }

    [DataContract]
    public class CriterionsContainer
    {

        [DataMember(Name = "criterions")]
        public Criterions Criterions { get; set; }

        [DataMember(Name = "sorting")]
        public Sorting Sorting { get; set; }

        [DataMember(Name = "limit")]
        public int Limit { get; set; }

        [DataMember(Name = "offset")]
        public int Offset { get; set; }

        [DataMember(Name = "total")]
        public int Total { get; set; }

        [DataMember(Name = "page")]
        public int Page { get; set; }

        [DataMember(Name = "page_count")]
        public int PageCount { get; set; }

        [DataMember(Name = "search_key")]
        public string SearchKey { get; set; }

        [DataMember(Name = "geo_children")]
        public IList<object> GeoChildren { get; set; }
    }


}
