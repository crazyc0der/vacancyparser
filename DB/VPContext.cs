﻿using DB.Models;
using System.Data.Entity;

namespace DB
{
    public class VPContext : DbContext
    {
        public VPContext() : base("VacancyDB")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<VPContext>());
        }

        public DbSet<Vacancy> Vacancies { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Contact> Contacts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }
    }
}
